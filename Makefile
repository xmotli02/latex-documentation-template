# FIT VUTBR - Documentation LaTeX template
# 
# Matus Motlik, xmotli02@stud.fit.vutbr.cz

NAME=dokumentace

all: $(NAME).tex
	latex $(NAME).tex
	bibtex $(NAME).aux
	latex $(NAME).tex
	latex $(NAME).tex
	dvips $(NAME).dvi
	ps2pdf -sPAPERSIZE=a4 $(NAME).ps

clean:
	rm -f $(NAME).aux $(NAME).log $(NAME).pdf $(NAME).dvi $(NAME).ps $(NAME).out $(NAME).aux.pdf $(NAME).bbl $(NAME).blg $(NAME).toc $(NAME).synctex.gz